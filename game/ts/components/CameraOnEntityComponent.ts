import Component from "../../../ts/engine/Component";
import { Point } from "pixi.js";
import { ATTR_BASE_ENTITY_MODEL, MSG_CMD_MOVE_CAMERA, CAMERA_OFFSET_WIDTH_COEF, CAMERA_OFFSET_HEIGHT_COEF, MSG_NOT_PLAYER_DEATH, MSG_CMD_ZOOM_CAMERA, MESSAGE_TIMEOUT } from "../Constants";
import { BaseEntityModel } from "../model/BaseEntityModel";
import Msg from "../../../ts/engine/Msg";

//! Component which sets up camera following the entity on which it is attached. It also zooms in slowly when death message is received
export class CameraOnEntityComponent extends Component {

    private lastPos = new Point(0, 0);
    private deathZoom = false;
    private deathZoomScale = 1;

    onInit() {
        this.subscribe(MSG_NOT_PLAYER_DEATH);
    }

    onRemove() {
        this.unsubscribe(MSG_NOT_PLAYER_DEATH);
    }

    onUpdate(delta: number, absolute: number) {
        let model = this.owner.getAttribute<BaseEntityModel>(ATTR_BASE_ENTITY_MODEL);
        let screenWidth = this.scene.app.screen.width;
        let screenHeight = this.scene.app.screen.height;
        let x = model.getX() - screenWidth * CAMERA_OFFSET_WIDTH_COEF;
        let y = model.getY() - screenHeight * CAMERA_OFFSET_HEIGHT_COEF;
        // Slow down camera changes
        this.lastPos.x = (this.lastPos.x + x) * 0.5;
        this.lastPos.y = (this.lastPos.y + y) * 0.5;
        this.sendMessage(MSG_CMD_MOVE_CAMERA, new Point(this.lastPos.x, this.lastPos.y));

        if (this.deathZoom) {
            // Apply zoom when dead gradually
            this.deathZoomScale *= (1 - delta / MESSAGE_TIMEOUT);
            this.sendMessage(MSG_CMD_ZOOM_CAMERA, this.deathZoomScale);
        }
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_NOT_PLAYER_DEATH) {
            if (msg.gameObject == this.owner) {
                this.deathZoom = true;
            }
        }
    }
}
