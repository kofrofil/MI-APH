// Texture ids
export const TEXTURE_BLOCK_GRASS = "TEXTURE_BLOCK_GRASS"; //< Grass block texture id
export const TEXTURE_BLOCK_STONE = "TEXTURE_BLOCK_STONE"; //< Stone block texture id
export const TEXTURE_BLOCK_WATER = "TEXTURE_BLOCK_WATER"; //< Water block texture id
export const TEXTURE_PLAYER = "TEXTURE_PLAYER"; //< Player sprite texture id
export const TEXTURE_LAVA = "TEXTURE_LAVA"; //< Lava sprite texture id
export const TEXTURE_FIRE = "TEXTURE_FIRE"; //< Fire sprite texture id 
export const TEXTURE_ROCK = "TEXTURE_ROCK"; //< Rock sprite texture id 


// Entity attributes
export const ATTR_BASE_ENTITY_MODEL = "ATTR_BASE_ENTITY_MODEL"; //< Abstract entity model with basic data
export const ATTR_PHYSICAL_ENTITY_MODEL = "ATTR_PHYSICAL_ENTITY_MODEL"; //< Concrete entity model with physics
export const ATTR_PLAYER_MODEL = "ATTR_PLAYER_MODEL"; //< Player model with player specific variables 
export const ATTR_GROUND_OPTIONS = "ATTR_GROUND_OPTIONS"; //< Ground generator options with specified starting and finishing heights


// Entity flags
export const FLAG_DESTRUCTIBLE = 1; //< SPecifies whether given entity is destructible and should be used in potential destruction collisions

// Message notifications
export const MSG_NOT_CAMERA_RECT_UPDATED = "MSG_NOT_CAMERA_RECT_UPDATED"; //< Notification about camera rectangle change
export const MSG_NOT_GROUND_CHANGED = "MSG_NOT_GROUND_CHANGED"; //< Notification about ground generator parameter changes

export const MSG_NOT_PHYSICAL_OBJECT_CREATED = "MSG_NOT_PHYSICAL_OBJECT_CREATED"; //< Notification about physical object creation (for registering in the phys. engine)
export const MSG_NOT_PHYSICAL_OBJECT_REMOVED = "MSG_NOT_PHYSICAL_OBJECT_REMOVED"; //< Notification about physical object destruciton (for unregistering in the phys. engine)

export const MSG_NOT_OBJECT_SCALE_CHANGED = "MSG_NOT_OBJECT_SCALE_CHANGED"; //< Notification about object scale change

export const MSG_NOT_COLLISION_START = "MSG_NOT_COLLISION_START"; //< Notification that collision start
export const MSG_NOT_COLLISION_ACTIVE = "MSG_NOT_COLLISION_ACTIVE"; //< Notification about an active collision
export const MSG_NOT_COLLISION_END = "MSG_NOT_COLLISION_END"; //< Notification about a collision end

export const MSG_NOT_ENTITY_DAMAGE = "MSG_NOT_ENTITY_DAMAGE"; //< Notification that entity is receiving some damage

export const MSG_NOT_PLAYER_HEALTH_CHANGED = "MSG_NOT_PLAYER_HEALTH_CHANGED"; //< Notification about player health change
export const MSG_NOT_PLAYER_DEATH = "MSG_NOT_PLAYER_DEATH"; //< Notification about player death
export const MSG_NOT_PLAYER_REACHED_FINISH = "MSG_NOT_PLAYER_REACHED_FINISH"; //< Notification that player has reached the finish (water)

export const MSG_LVL_UPDATED = "MSG_LVL_UPDATED"; //< Notification about level change


// Message commands
export const MSG_CMD_SETUP_GROUND = "MSG_CMD_SETUP_GROUND"; //< Command to setup and populate ground
export const MSG_CMD_MOVE_CAMERA = "MSG_CMD_MOVE_CAMERA"; //< Command for camera to change position
export const MSG_CMD_ZOOM_CAMERA = "MSG_CMD_ZOOM_CAMERA"; //< Command for camera to zoom around its center
export const MSG_CMD_RESET_CAMERA = "MSG_CMD_RESET_CAMERA"; //< Command to reset stored camera state
export const MSG_CMD_SETUP_HUD = "MSG_CMD_SETUP_HUD"; //< Command to create and setup HUD


// General "magic" constant adjustments
export const BACKGROUND_COLOR = 0x5599CC; //< Background clear color
export const BLOCK_SIZE = 8; //< Block rectangle side pixels
export const FIRE_SIZE = 8; //< Fire particle side pixels
export const MESSAGE_TIMEOUT = 3; //< HUD message timeout in seconds

export const LAVA_INIT_POS_X = -400; //< Initial lava center position x
export const LAVA_INIT_POS_Y = -250; //< Initial lava center position y
export const LAVA_WIDTH = 400; //< Lava object width
export const LAVA_HEIGHT = 300; //< Lava object height
export const LAVA_Y_OFFSET = 100; //< Lava Y offset from ground
export const LAVA_POINTS_X = 40; //< Lava rope adjustible points on X axis and tiling multiplier
export const LAVA_POINTS_Y = 30; // Lava rope adjustible points on Y axis, used for tiling multipler only, rope cannot be adjusted in Y
export const LAVA_DAMAGE_PER_SECOND = 0.6; //< Damage lava inflicts every second, 1 being full player health
export const LAVA_HURRY_CHEAT = 0.15; //< Factor speeding up lava based on camera distance from it. Increases difficulty.
export const LAVA_SMOOTHING = 0.15; //< Factor smoothing lava rope height difference
export const LAVA_SPEED = 42; //< Base speed at which lava moves on X axis in pixels per second
export const LAVA_UNDERGROUND_ROT_COEF = 0.6;  //< How much is lava tilted towards ground (physically correct calculation factor to account for noise)
export const LAVA_FINISH_HEIGHT_DIST = LAVA_HEIGHT * 0.4; //< Height distance offset at which lava stops before the finish (Water)

export const WATER_PHYS_OFFSET = 20; //< How much pixels player sinks into water

export const ROCK_SIZE = 32; //< Rock texture side size in pixels
export const ROCK_TIME_TO_LIVE = 6; //< ROck time to live existence in seconds from spawning
export const ROCK_SPAWN_TIMEOUT = 0.9; //< Time to wait before rock spawn.
export const ROCK_SHOT_FORCE_X = 0.03; //< Impulce force as if it was applied for 1 second X direction
export const ROCK_SHOT_FORCE_Y = 0.01; //< Impulce force as if it was applied for 1 second Y direction
export const ROCK_MASS = 60; //< Rock mass
export const ROCK_SPEED_PARTICLE_SPAWN_FACTOR = 0.001; //< Speed factor at which fire particles are spawned

export const PLAYER_INIT_POS_X = -100; //< Initial player position on X axis
export const PLAYER_INIT_POS_Y = -250; //< Initial player position on Y axis
export const PLAYER_WIDTH = 40; //< Player sprite width in pixels (animated sub-frame)
export const PLAYER_STOPING_FORCE_COEF = 0.00004; //< Player velocity dampening when not being controlled coefficient (not physically correct)
export const PLAYER_STOPING_VELOCITY_THRESHOLD = 0.000001; //< Player velocity threshold at which to dampen it.
export const PLAYER_JUMP_TIMEOUT = 0.5; //< Timeout before player can jump again.
export const PLAYER_JUMP_IMPULSE_FORCE_SIDEWAY = 0.00006; //< Player jumping force X axis
export const PLAYER_JUMP_IMPULSE_FORCE_UP = 0.00023; //< Player jumping force Y axis
export const PLAYER_MOVE_FORCE_SIDEWAYS = 0.000018; //< Player movement force applied from controls on X axis when moving sideways
export const PLAYER_MOVE_FORCE_UP = 0.000009; //< Player movement force applied from controls on Y axis when moving sideways
export const PLAYER_MASS = 1; //< Mass of player
export const PLAYER_COLLISION_DAMAGE = 0.01; //< Player damage from any object collision (simulates lava rock damage)

export const GRAVITY = 1 / 2.0; //< Matter-js gravity setting
export const CAMERA_OFFSET_WIDTH_COEF = 0.45; //< Player width offset from left (1 = full width)
export const CAMERA_OFFSET_HEIGHT_COEF = 0.6; //< Player width offset from top (1 = full height)

export const ANIMATION_VELOCITY_THRESHOLD = 0.2; //< Velocity threshold at which to animate.
export const ANIMATION_SPEED = 10; //< Animation frames per second 

export const PARTICLES_PER_CONTAINER = 200; //< Max particles per particle container (for each entity / particle component)
export const PARTICLES_PER_SECOND = 100; //< Max particles per spawned per second (for each entity / particle component)

// Ground default options
export const GROUND_NOISE_SLOPE = -0.3; //< Slope factor for ground steepiness Y = X * slope
export const GROUND_NOISE_PERSISTENCE = 0.6; //< Noise parameter for persistence of octaves (iteratios)
export const GROUND_NOISE_OCTAVES = 3; //< Noise parameter for number of iterations
export const GROUND_NOISE_ZOOM = 10; //< Zooming factor on input coordinates
export const GROUND_NOISE_SCALE = 2; //< Separate block scaling on input
export const GROUND_NOISE_AMPLITUDE  = 3; //< Noise output scaling (maximum amplitude of the noise)
export const GROUND_NOISE_SEED = 0; //< Noise pseudo random seed parameter
export const GROUND_STARTING_HEIGHT = 0; //< Height at which ground generates    
export const GROUND_FINISH_HEIGHT = 700; //< Height at which ground generation stops and is filled with water
export const GROUND_STONE_GRASS_OFFSET = 10; //< Height of grass above stone

// Key constants
export const KEY_LEFT = 37; //< Left key-code
export const KEY_RIGHT = 39; //< Right key-code
export const KEY_SPACE = 32; //< Space key-code
export const KEY_A = 65; //< A key-code
export const KEY_D = 68; //< D key-code
