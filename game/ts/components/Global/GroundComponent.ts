import { Rectangle } from "pixi.js";
import { MSG_NOT_CAMERA_RECT_UPDATED, BLOCK_SIZE, MSG_NOT_GROUND_CHANGED, MSG_CMD_SETUP_GROUND, WATER_PHYS_OFFSET } from "../../Constants";
import { Noise } from "../../Noise";
import { BlockType, GroundRow, Block, GroundColumn, GroundChange, GroundOptions, GroundGenerator } from "../../GroundParts";
import { Utils } from "../../Utils";
import Component from "../../../../ts/engine/Component";
import Msg from "../../../../ts/engine/Msg";

//! Global component populates ground around player from configured noise via streaming.
export class GroundComponent extends Component
{
    //! Row holding columns of blocks
    private row : GroundRow = null;
    //! Block generator instance recreated for each new ground options
    private generator : GroundGenerator = null;
    private blockPool = new Map<BlockType, Block[]>();

	onInit() {
        this.subscribe(MSG_NOT_CAMERA_RECT_UPDATED);
        this.subscribe(MSG_CMD_SETUP_GROUND);
	}

	onUpdate(delta: number, absolute: number) {
    }

    onRemove() {
        this.unsubscribe(MSG_NOT_CAMERA_RECT_UPDATED);
        this.unsubscribe(MSG_CMD_SETUP_GROUND); 
    }

	onMessage(msg: Msg) {
        if (msg.action == MSG_NOT_CAMERA_RECT_UPDATED) {
            // Setup ground at new rectangle
            this.populateScreen((<Rectangle>msg.data));
        }
        if (msg.action == MSG_CMD_SETUP_GROUND) {
            // Reset ground based on new ground options
            this.cleanup();
            this.row = new GroundRow;
            let options = <GroundOptions>msg.data;
            this.owner.getScene().stage.getPixiObj().addChild(this.row);
            this.generator = new GroundGenerator(options);
            this.populateScreen(new Rectangle(0, 0, this.owner.getScene().app.screen.width, this.owner.getScene().app.screen.height));
        }
    }

    //! Returns currently used ground generator
    getGroundGenerator() : GroundGenerator {
        return this.generator;
    }

    //! Clean al ground columns and announce the change (to get unregistered from physics engine)
    private cleanup() {
        let columnsToRemove : GroundColumn[] = [];

        if (!this.row) {
            return;
        }

        for (let columnAny of this.row.children) {
            let column : GroundColumn = <GroundColumn>columnAny;
            columnsToRemove.push(column);
        }

        for (let columnAny of columnsToRemove) {
            let column : GroundColumn = <GroundColumn>columnAny;
            this.recyleColumn(column);
        }
        this.broadcastGroundChange(this.getColumnRectangles(columnsToRemove), null);
    }

    //! Takes a block from block cache or creates new one
    private getNewBlock(type : BlockType) : Block {
        let blocks = this.blockPool.get(type);
        if (blocks != null && blocks.length > 0) {
            return blocks.splice(blocks.length - 1, 1)[0];
        }
        return new Block(type);
    }

    //! Populates given column for give camera rectangle with some margin
    private populateColumn(column : GroundColumn, rect : Rectangle) {
        let marginoFfset = 0.5;
        let marginScale = 2.0;
        let maxBlocks = ((Math.max(rect.width, rect.height) * marginScale) / BLOCK_SIZE) | 0;
        let columnTopScreen = (rect.y / BLOCK_SIZE) | 0;
        let columnTopBlock = columnTopScreen - maxBlocks * marginoFfset / marginScale;

        let types = new Array<BlockType>(maxBlocks);
        this.generator.fillColumnTypes(column.position.x, columnTopBlock * BLOCK_SIZE, types);

        for (let i = 0 | 0; i < maxBlocks; i += 1 | 0) {
            if (types[i] != BlockType.Air) {
                let block = this.getNewBlock(types[i]);
                block.position.y = columnTopBlock * BLOCK_SIZE + i * BLOCK_SIZE;
                column.addChild(block);
            }
        }
    }

    //! Cache blocks from not needed column
    private recyleColumn(column : GroundColumn) {
        let recycled = 0;
        for (let blockAny of column.children) {
            let block = <Block>blockAny;

            if (this.blockPool.get(block.type) == null) {
                this.blockPool.set(block.type, new Array<Block>());
            }

            this.blockPool.get(block.type).push(block);
        }

        if (this.blockPool.get(BlockType.Stone)) {
        }
    }

    //! Adjusts ground columns to fit into current camera rectange, creating/removing them and announcing changes via msg.
    private populateScreen(rect : Rectangle) {
        if (this.generator == null|| this.row == null) {
            return;
        }
        // Present columns relative to current camera rect.
        let newColumnArray : boolean[] = [];
        for (let i = 0; i < (rect.width / BLOCK_SIZE) + 3; i++) {
            newColumnArray[i | 0] = false;
        }

        // Mark present columns relative to current camera rect and find columns for removal.
        let columnsToRemove : GroundColumn[] = [];
        for (let columnAny of this.row.children) {
            let column : GroundColumn = <GroundColumn>columnAny;
            let relativeIndex = Math.floor((-rect.x + this.row.position.x + column.position.x) / BLOCK_SIZE) | 0;
            // Remove columns completely outside of window
            if (relativeIndex < -1 || relativeIndex >= newColumnArray.length) {
                columnsToRemove.push(column);
            } else {
                newColumnArray[relativeIndex] = true;
            }
        }

        // Simple optimization
        let groundChanged : boolean = columnsToRemove.length > 0;

        for (let columnAny of columnsToRemove) {
            let column : GroundColumn = <GroundColumn>columnAny;
            this.row.removeChild(column);
            this.recyleColumn(column);
        }

        if (groundChanged) {
            this.broadcastGroundChange(this.getColumnRectangles(columnsToRemove), null);
        }

        let columnsNew : GroundColumn[] = [];

        // Create missing columns
        for (let i = 0; i < (rect.width / BLOCK_SIZE) + 2; i++) {
            if (!newColumnArray[i | 0]) {
                let column = new GroundColumn;
                column.position.x = ((Math.floor(rect.x / BLOCK_SIZE) + i) * BLOCK_SIZE);
                this.row.addChild(column);
                columnsNew.push(column);
                this.populateColumn(column, rect);
                groundChanged = true;
            }
        }

        if (groundChanged) {
            this.broadcastGroundChange(null, this.getColumnRectangles(columnsNew));
        }
    }

    //! Returns rectangles of given columns. These are aggregated to larger rectangles (just in columns)
    private getColumnRectangles(columns : GroundColumn[]) : Rectangle[] {
        let rectangles : Rectangle[] = [];

        for (let columnAny of columns) {
            let column : GroundColumn = <GroundColumn>columnAny;

            let rect : Rectangle = null;
            let prevBlock : Block = null;

            for (let blockAny of column.children) {
                let block = <Block>blockAny;
                
                let blockY = block.position.y;
                if (block.type == BlockType.Water) {
                    blockY += WATER_PHYS_OFFSET;
                }

                if (rect != null) {
                    let prevBlockY = prevBlock.position.y;
                    if (prevBlock.type == BlockType.Water) {
                        prevBlockY += WATER_PHYS_OFFSET;
                    }
                    // concat rects
                    if (Utils.almostSameNumber(block.position.x, prevBlock.position.x) && Utils.almostSameNumber(blockY, prevBlockY + prevBlock.height)) {
                        rect.height += block.height;
                    } else {
                        rectangles.push(rect);
                        rect = null;
                        console.log("Block broken");
                    }
                }

                if (rect == null) {
                    // Blocks are improperly centered to allow random rotation, TODO: Render sprites correctly.
                    rect = new Rectangle(this.row.position.x + column.position.x + block.position.x - block.width * 0.5, this.row.position.y + column.position.y + blockY - block.height * 0.5, block.width, block.height);
                }

                prevBlock = block;
            }
            if (rect) {
                rectangles.push(rect);
            }
        }

        return rectangles;
    }

    private broadcastGroundChange(columnsRemoved : Rectangle[], columnsNew : Rectangle[]) {

        this.sendMessage(MSG_NOT_GROUND_CHANGED, new GroundChange(columnsRemoved, columnsNew));
    }
}
