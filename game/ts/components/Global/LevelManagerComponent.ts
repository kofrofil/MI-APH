import Component from "../../../../ts/engine/Component";
import { SceneFactory } from "../../SceneFactory";
import { MSG_CMD_SETUP_GROUND, MSG_CMD_SETUP_HUD, MSG_NOT_PLAYER_DEATH, ATTR_GROUND_OPTIONS, MSG_NOT_PLAYER_REACHED_FINISH, MSG_LVL_UPDATED, MESSAGE_TIMEOUT, MSG_CMD_RESET_CAMERA } from "../../Constants";
import { GroundOptions } from "../../GroundParts";
import Msg from "../../../../ts/engine/Msg";

//! Component handling levels when player dies or reaches finish, it also sets difficult by adjusting ground options (it is the primary source of it)
export class LevelManagerComponent extends Component {

    //! Current level, it has to be initialized the first frame when all components are ready
    private currentLevel : number = -1;
    private sceneFactory : SceneFactory = null;
    //! When positive gives the time when level will start again
    private restartInTimeout : number = 0;
    //! When positive gives the time when level will increase and start
    private nextLevelInTimeout : number = 0;
    //! When setup is requested, this has to happen during update to allow systems to process the end
    private finishSetup = false;

    onInit() {
        this.sceneFactory = new SceneFactory(this.scene);
        this.subscribe(MSG_NOT_PLAYER_DEATH);
        this.subscribe(MSG_NOT_PLAYER_REACHED_FINISH);
    }

    onRemove() {
        this.unsubscribe(MSG_NOT_PLAYER_DEATH);
        this.unsubscribe(MSG_NOT_PLAYER_REACHED_FINISH);
    }

    //! Sets up
    private prepareLevelSetup() {
        this.scene.clearEntities();
        let groundOptions = new GroundOptions;
        if (this.currentLevel > 0) {
            groundOptions.amplitude *= this.currentLevel;
            groundOptions.seed = this.currentLevel - 1;
        }
        this.scene.removeGlobalAttribute(ATTR_GROUND_OPTIONS);
        this.scene.addGlobalAttribute(ATTR_GROUND_OPTIONS, groundOptions);

        this.sendMessage(MSG_CMD_RESET_CAMERA);
        this.finishSetup = true;
    }

    onUpdate(delta: number, absolute: number) {
        // Setup needs to be finished in update as some systems need to have the correct camera rectangle already
        if (this.finishSetup) {
            // Order here will reflect the rendering order as well.
            this.sceneFactory.spawnPlayer();
            this.sceneFactory.spawnLava();
            let groundOptions = this.scene.getGlobalAttribute<GroundOptions>(ATTR_GROUND_OPTIONS);
            this.sendMessage(MSG_CMD_SETUP_GROUND, groundOptions);
            this.sendMessage(MSG_CMD_SETUP_HUD);
            this.finishSetup = false;
        }

        // First level setup needs to occur once all components are initialized to receive all messages
        if (this.currentLevel < 0) {
            this.currentLevel = 1;
            this.prepareLevelSetup();
        }

        if (this.restartInTimeout > 0) {
            this.restartInTimeout -= delta;
            if (this.restartInTimeout <= 0) {
                this.prepareLevelSetup();
            }
        }

        if (this.nextLevelInTimeout > 0) {
            this.nextLevelInTimeout -= delta;
            if (this.nextLevelInTimeout <= 0) {
                this.currentLevel += 1;
                this.sendMessage(MSG_LVL_UPDATED, this.currentLevel);
                this.prepareLevelSetup();
            }
        }
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_NOT_PLAYER_DEATH && this.restartInTimeout <= 0 && this.nextLevelInTimeout <= 0) {
            this.restartInTimeout = MESSAGE_TIMEOUT;
        }
        if (msg.action == MSG_NOT_PLAYER_REACHED_FINISH && this.nextLevelInTimeout <= 0 && this.restartInTimeout <= 0) {
            this.nextLevelInTimeout = MESSAGE_TIMEOUT;
        }
    }
}
