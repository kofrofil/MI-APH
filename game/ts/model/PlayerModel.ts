//! Basic player entity data (next to entity model)
export class PlayerModel {
    health : number;

    constructor() {
        this.reset();
    }

    reset() {
        this.health = 1;
    }
}
