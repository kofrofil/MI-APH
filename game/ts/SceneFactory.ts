import { PIXICmp } from "../../ts/engine/PIXIObject";
import { TEXTURE_PLAYER, ATTR_BASE_ENTITY_MODEL, ATTR_PHYSICAL_ENTITY_MODEL, TEXTURE_LAVA, LAVA_WIDTH, LAVA_HEIGHT, LAVA_POINTS_X, FLAG_DESTRUCTIBLE, LAVA_INIT_POS_X, LAVA_INIT_POS_Y, PLAYER_INIT_POS_X, PLAYER_INIT_POS_Y, ATTR_PLAYER_MODEL, TEXTURE_ROCK, BLOCK_SIZE, ROCK_SIZE, PLAYER_MASS, ROCK_MASS, } from "./Constants";
import PIXIObjectBuilder from "../../ts/engine/PIXIObjectBuilder";
import Scene from "../../ts/engine/Scene";
import { PhysicalEntityModel } from "./model/PhysicalEntityModel";
import { PhysicalEntityComponent } from "./components/PhysicalEntityComponent";
import { CameraOnEntityComponent } from "./components/CameraOnEntityComponent";
import { SpriteAnimationComponent } from "./components/SpriteAnimationComponent";
import { PlayerControllerComponent } from "./components/PlayerControllerComponent";
import { LavaEntityModel } from "./model/LavaEntityModel";
import { Point, WRAP_MODES } from "pixi.js";
import { LavaAnimationComponent } from "./components/LavaAnimationComponent";
import { LavaDestructionComponent } from "./components/LavaDestructionComponent";
import { FireParticleSpawnerComponent } from "./components/FireParticleSpawnerComponent";
import { PlayerModel } from "./model/PlayerModel";
import { RockShotComponent } from "./components/RockShotSimulComponent";
import { RockSpawnerComponent } from "./components/RockSpawnerComponent";

//! Factory for entity creation in the scene
export class SceneFactory {
    private scene : Scene = null;

    constructor(scene : Scene) {
        this.scene = scene;
    }

    //! Spawns player entity on its default position
    spawnPlayer() {
        let playerSprite = new PIXICmp.Sprite(TEXTURE_PLAYER, PIXI.Texture.fromImage(TEXTURE_PLAYER));
        let physicalModel = new PhysicalEntityModel(playerSprite);
        let playerModel = new PlayerModel();
        new PIXIObjectBuilder(this.scene)
            .globalPos(PLAYER_INIT_POS_X, PLAYER_INIT_POS_Y)
            .anchor(0.5, 0.5)
            .scale(0.01, 0.01) // TODO: HAAAX Workaround for too early physics registration
            .withAttribute(ATTR_BASE_ENTITY_MODEL, physicalModel)
            .withAttribute(ATTR_PHYSICAL_ENTITY_MODEL, physicalModel)
            .withAttribute(ATTR_PLAYER_MODEL, playerModel)
            .withFlag(FLAG_DESTRUCTIBLE)
            .withComponent(new PhysicalEntityComponent(PLAYER_MASS))
            .withComponent(new CameraOnEntityComponent)
            .withComponent(new SpriteAnimationComponent)
            .withComponent(new PlayerControllerComponent)
            .withComponent(new FireParticleSpawnerComponent)
            .build(playerSprite, this.scene.stage);
    }

    //! Spawns lava entity on its default position
    spawnLava() {
        let tex = PIXI.Texture.fromImage(TEXTURE_LAVA);
        let points: Point[] = [];

        // Fill rope points for later animation control
        for (let i = 0; i < LAVA_POINTS_X; i++) {
            points.push(new Point(i * tex.baseTexture.width, 0));
        }
        let lavaSprite = new PIXICmp.Rope(TEXTURE_LAVA, tex, points);

        // allow the sprite to be tiled
        lavaSprite.texture.baseTexture.wrapMode = WRAP_MODES.REPEAT;

        // Scale according to config
        let widthScale = LAVA_WIDTH / (tex.baseTexture.width * (LAVA_POINTS_X - 1));
        let heightScale = LAVA_HEIGHT / tex.baseTexture.height;;

        let lavaModel = new LavaEntityModel(lavaSprite);
        new PIXIObjectBuilder(this.scene)
            .globalPos(- LAVA_WIDTH * 1.2 + LAVA_INIT_POS_X, LAVA_INIT_POS_Y)
            .scale(widthScale, heightScale)
            .withAttribute(ATTR_BASE_ENTITY_MODEL, lavaModel)
            .withComponent(new LavaAnimationComponent)
            .withComponent(new LavaDestructionComponent)
            .withComponent(new RockSpawnerComponent)
            .build(lavaSprite, this.scene.stage);
    }

    //! Spawns rock entity on its default position where an impulse force will be applied
    spawnRock(position: Point, impulseForce: Point) {

        let tex = PIXI.Texture.fromImage(TEXTURE_ROCK);
        let rockSprite = new PIXICmp.Sprite(TEXTURE_ROCK, tex);
        let physicalModel = new PhysicalEntityModel(rockSprite);

        // Scale according to config
        let sizeRandom = Math.random();
        let size = sizeRandom * ROCK_SIZE;
        let widthScale = size / tex.baseTexture.width;
        let heightScale = size / tex.baseTexture.height;

        new PIXIObjectBuilder(this.scene)
            .globalPos(position.x, position.y)
            .anchor(0.5, 0.5)
            .scale(widthScale, heightScale)
            .withAttribute(ATTR_BASE_ENTITY_MODEL, physicalModel)
            .withAttribute(ATTR_PHYSICAL_ENTITY_MODEL, physicalModel)
            .withFlag(FLAG_DESTRUCTIBLE)
            .withComponent(new PhysicalEntityComponent(ROCK_MASS * sizeRandom)) // Mass is not physically correct this way
            .withComponent(new FireParticleSpawnerComponent)
            .withComponent(new RockShotComponent(impulseForce))
            .build(rockSprite, this.scene.stage);
    }
}
