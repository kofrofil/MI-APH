import { TEXTURE_BLOCK_WATER, TEXTURE_BLOCK_GRASS, TEXTURE_BLOCK_STONE, BLOCK_SIZE, GROUND_STONE_GRASS_OFFSET, GROUND_NOISE_SLOPE, GROUND_NOISE_PERSISTENCE, GROUND_NOISE_OCTAVES, GROUND_NOISE_ZOOM, GROUND_NOISE_SCALE, GROUND_NOISE_AMPLITUDE, GROUND_NOISE_SEED, GROUND_STARTING_HEIGHT, GROUND_FINISH_HEIGHT } from "./Constants";
import { Texture, Rectangle, Point } from "pixi.js";
import { PIXICmp } from "../../ts/engine/PIXIObject";
import { Noise } from "./Noise";

//! Block types used in ground representation
export enum BlockType {
    Air, //< Transparent, is never drawn.
    Grass, //< Grass block (physical interaction)
    Stone, //< Stone block (physical interaction)
    Water //< Water block (physical interaction with offset)
}

//! Optimized block sprite (not using component object to improve engine performance)
export class Block extends PIXI.Sprite {
    //! block type for later distinction
    type : BlockType;

    //! Sets up block with texture for given type and randomly orientates it
    constructor(type : BlockType) {
        super();

        this.type = type;

        let textureId = "";
        switch (type) {
            case BlockType.Air: Error("Invalid block type"); textureId = TEXTURE_BLOCK_WATER; break;
            case BlockType.Grass: textureId = TEXTURE_BLOCK_GRASS; break;
            case BlockType.Stone: textureId = TEXTURE_BLOCK_STONE; break;
            case BlockType.Water: textureId = TEXTURE_BLOCK_WATER; break;
        }
        this.texture = Texture.fromImage(textureId);

        // Random orientation to create random pattern among blocks
        this.anchor.x = 0.5;
        this.anchor.y = 0.5;
        this.rotation = Math.floor(Math.random() * 4) * 0.5 * Math.PI;
        this.scale.x = (Math.floor(Math.random()) - 0.5) * 2.0 * BLOCK_SIZE / this.texture.baseTexture.width;
        this.scale.y = (Math.floor(Math.random()) - 0.5) * 2.0 * BLOCK_SIZE / this.texture.baseTexture.height;
    }
}

//! Helper container for blocks in column, used to position in X axis
export class GroundColumn extends PIXICmp.Container {
}

//! Helper container for ground columns to be offset a little bit in the scene
export class GroundRow extends PIXICmp.Container {

    constructor() {
        super();

        // Offset all blocks to right to align block begining with coordinate system.
        this.position.x = BLOCK_SIZE / 2;
        this.position.y = BLOCK_SIZE / 2;
    }
}

//! Helper class with bounding rectangles of newly streamed in and deleted ground columns 
export class GroundChange {
    constructor(removed : Rectangle[], added : Rectangle[]) {
        this.removed = removed;
        this.added = added;
    }

    //! Bounding rectangles of deleted columns
    removed : Rectangle[] = null;
    //! Bounding rectangles of added columns
    added : Rectangle[] = null;
}

//! Stores parameters / options for ground generation.
export class GroundOptions {
    //! Slope factor for ground steepiness Y = X * slope
    slope : number = GROUND_NOISE_SLOPE;
    //! Noise parameter for persistence of octaves (iteratios)
    persistence : number = GROUND_NOISE_PERSISTENCE;
    //! Noise parameter for number of iterations
    octaves : number = GROUND_NOISE_OCTAVES;
    //! Zooming factor on input coordinates
    zoom : number = GROUND_NOISE_ZOOM;
    //! Separate block scaling on input
    scale : number = GROUND_NOISE_SCALE;
    //! Noise output scaling (maximum amplitude of the noise)
    amplitude : number = GROUND_NOISE_AMPLITUDE;
    //! Noise pseudo random seed parameter
    seed : number = GROUND_NOISE_SEED;
    //! Height at which ground generates
    startingHeight : number = GROUND_STARTING_HEIGHT;
    //! Height at which ground generation stops and is filled with water
    finishHeight : number = GROUND_FINISH_HEIGHT;
}

//! Generates block type heights for given ground options and X coordinate
export class GroundGenerator {
    private options : GroundOptions = null;
    private noise : Noise = new Noise;

    constructor(options : GroundOptions) {
        this.options = options;
        this.noise.seed(this.options.seed);
    }

    getOptions() : GroundOptions {
        return this.options;
    }

    //! Returns height difference of { grass, stone } at current x coordinate from current ground level
    getGroundDiffsAt(x : number) : Point {
        let groundLevel = x * -this.options.slope;
        let smoothAmplitudeStart = groundLevel - this.options.startingHeight;
        smoothAmplitudeStart = Math.min(smoothAmplitudeStart, this.options.amplitude);
        smoothAmplitudeStart = Math.max(smoothAmplitudeStart, 0);

        let smoothAmplitudeEnd = this.options.finishHeight - groundLevel;
        smoothAmplitudeEnd = Math.min(smoothAmplitudeEnd, this.options.amplitude);
        smoothAmplitudeEnd = Math.max(smoothAmplitudeEnd, 0);
    
        let smoothAmplitude = Math.min(smoothAmplitudeStart, smoothAmplitudeEnd);
        let noiseStoneVal = smoothAmplitude * BLOCK_SIZE * this.noise.calcPoint(x / BLOCK_SIZE / this.options.scale, 0, this.options.persistence, this.options.octaves, this.options.zoom);
        let noiseGrassVal = smoothAmplitude * BLOCK_SIZE * this.noise.calcPoint(x / BLOCK_SIZE / this.options.scale, 0.5, this.options.persistence, this.options.octaves, this.options.zoom);
        let groundLevelStone = noiseStoneVal + GROUND_STONE_GRASS_OFFSET;
        let groundLevelGrass = noiseGrassVal;

        return new Point(groundLevelGrass, groundLevelStone);
    }

    //! Populates blockTypes array from relative yMin height for given x coordinate
    fillColumnTypes(x : number, yMin : number, blockTypes : BlockType[]) {

        let groundLevel = x * -this.options.slope;
        let groundDiffs = this.getGroundDiffsAt(x);

        for (let i = 0 | 0; i < blockTypes.length; i += 1 | 0)
        {
            let type = BlockType.Air;
            let currentHeight = i * BLOCK_SIZE + yMin;
            if (currentHeight > groundDiffs.x + groundLevel)
            {
                type = BlockType.Grass;
            }
            if (currentHeight > groundDiffs.y + groundLevel)
            {
                type = BlockType.Stone;
            }
            if (currentHeight > this.options.finishHeight)
            {
                type = BlockType.Stone;
            }
            if (currentHeight > this.options.finishHeight && groundLevel > this.options.finishHeight)
            {
                type = BlockType.Water;
            }
            blockTypes[i] = type;
        }
    }
}
