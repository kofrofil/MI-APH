import Component from "../../../ts/engine/Component";
import { LAVA_POINTS_X, LAVA_POINTS_Y, ATTR_BASE_ENTITY_MODEL, LAVA_SPEED, MSG_CMD_SETUP_GROUND, LAVA_WIDTH, LAVA_Y_OFFSET, LAVA_HEIGHT, MSG_CMD_MOVE_CAMERA, LAVA_HURRY_CHEAT, LAVA_UNDERGROUND_ROT_COEF, LAVA_FINISH_HEIGHT_DIST, LAVA_SMOOTHING } from "../Constants";
import { Noise } from "../Noise";
import { BaseEntityModel } from "../model/BaseEntityModel";
import Msg from "../../../ts/engine/Msg";
import { GroundOptions, GroundGenerator } from "../GroundParts";
import { Point } from "pixi.js";

//! Animates (and moves) lava sprite rope points according to ground.
export class LavaAnimationComponent extends Component {
    //! Announced ground options
    private groundOptions : GroundOptions = new GroundOptions;
    //! Ground genererator built from above options
    private groundGenerator : GroundGenerator = new GroundGenerator(this.groundOptions);
    //! Camera offset for making 
    private cameraOffset : number = 0;

    onInit() {
        let lavaSprite = <PIXI.mesh.Rope>this.owner.getPixiObj();
        // Setup uvs for triangle strip vertices
        lavaSprite.uvs = new Float32Array(lavaSprite.vertices.length);

        // Adjust each UV coordinate to tile lava texture in LAVA_POINTS_X x LAVA_POINTS_Y grid.
        for (let i = 0; i < lavaSprite.uvs.length / 2; i++) {
            let u = i / (lavaSprite.uvs.length / 2) * LAVA_POINTS_X;
            lavaSprite.uvs[i * 2] = u;
            if (i % 2 == 1) {
                lavaSprite.uvs[i * 2 + 1] = 0;
            } else {
                lavaSprite.uvs[i * 2 + 1] = LAVA_POINTS_Y;
            }
        }

        this.subscribe(MSG_CMD_SETUP_GROUND);
        this.subscribe(MSG_CMD_MOVE_CAMERA);
    }

    //! Adjusts each rope point according to ground slope and generate difference noise from ground
    private animatePoints(delta: number) {
        let lavaSprite = <PIXI.mesh.Rope>this.owner.getPixiObj();
        let model = this.owner.getAttribute<BaseEntityModel>(ATTR_BASE_ENTITY_MODEL);
        for (let i = 0; i < lavaSprite.points.length; i++) {
            let x = i * LAVA_WIDTH / (LAVA_POINTS_X - 1) + model.getX() - model.getWidth() * 0.5;
            let heights = this.groundGenerator.getGroundDiffsAt(x);
            lavaSprite.points[i].y = heights.x * LAVA_SMOOTHING;
        }
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_CMD_SETUP_GROUND) {
            this.groundOptions = <GroundOptions>msg.data;
            this.groundGenerator = new GroundGenerator(this.groundOptions);
        }
        if (msg.action == MSG_CMD_MOVE_CAMERA) {
            let point = <Point>msg.data;
            this.cameraOffset = point.x;
        }
    }

    //! Actual animation simulation
    onUpdate(delta: number, absolute: number) {
        let lavaSprite = <PIXI.mesh.Rope>this.owner.getPixiObj();
        // Calculate lava cheat to follow too fast player
        let cheat = (this.cameraOffset - lavaSprite.position.x) * LAVA_HURRY_CHEAT;

        // Slow down lava when reaching water
        let proximityToWater = Math.abs(lavaSprite.position.y - this.groundOptions.finishHeight + LAVA_FINISH_HEIGHT_DIST);
        let slowDownCoefNearFinish = 1;
        if (proximityToWater < LAVA_HEIGHT) {
            let ratio = proximityToWater / LAVA_HEIGHT;
            slowDownCoefNearFinish = Math.sqrt(Math.sqrt(ratio));
        }
        // Stop down lava when it reaches water
        if (lavaSprite.position.y > this.groundOptions.finishHeight - LAVA_FINISH_HEIGHT_DIST) {
            slowDownCoefNearFinish = 0;
        }

        // Set animated lava position according to the ground slope and simply time
        lavaSprite.position.x += (LAVA_SPEED + cheat) * delta * slowDownCoefNearFinish;
        lavaSprite.position.y = lavaSprite.position.x * -this.groundOptions.slope;

        let model = this.owner.getAttribute<BaseEntityModel>(ATTR_BASE_ENTITY_MODEL);
    
        // Hide the lava front under terrain
        let a = model.getWidth() * 0.5;
        let b = model.getHeight() * 0.5
        let c = Math.sqrt(a * a + b * b);
        lavaSprite.rotation = Math.atan(-this.groundOptions.slope) + Math.acos(a / c) * LAVA_UNDERGROUND_ROT_COEF;

        this.animatePoints(delta);
    }

    onRemove() {
        this.unsubscribe(MSG_CMD_SETUP_GROUND);
        this.unsubscribe(MSG_CMD_MOVE_CAMERA);
    }
}
