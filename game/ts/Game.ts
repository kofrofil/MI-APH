import { PixiRunner } from '../../ts/PixiRunner'
import { TEXTURE_BLOCK_GRASS, TEXTURE_BLOCK_STONE, TEXTURE_BLOCK_WATER, BACKGROUND_COLOR, TEXTURE_PLAYER, TEXTURE_LAVA, TEXTURE_FIRE, TEXTURE_ROCK } from './Constants';
import { Texture, SCALE_MODES } from 'pixi.js';
import { InputComponent } from './components/Global/InputComponent';
import { PhysicsEngineComponent } from './components/Global/PhysicsEngineComponent';
import { CameraComponent } from './components/Global/CameraComponent';
import { GroundComponent } from './components/Global/GroundComponent';
import { LevelManagerComponent } from './components/Global/LevelManagerComponent';
import { HUDComponent } from './components/Global/HUDComponent';

//! Whole game instance
class Game {
    // List of used block textures, for special processing.
    private blockTextures = [
                                [TEXTURE_BLOCK_GRASS, "static/game/Grass.png"],
                                [TEXTURE_BLOCK_STONE, "static/game/Stone.png"],
                                [TEXTURE_BLOCK_WATER, "static/game/Water.png"]
                            ];

    //! Initialize pixi-js and its runner
    constructor() {
        this.engine = new PixiRunner();
        this.engine.init(document.getElementById("gameCanvas") as HTMLCanvasElement, 4);

        PIXI.loader
            .reset()    // necessary for hot reload
            .add(TEXTURE_BLOCK_GRASS, "static/game/Grass.png")
            .add(TEXTURE_BLOCK_STONE, "static/game/Stone.png")
            .add(TEXTURE_BLOCK_WATER, "static/game/Water.png")
            .add(TEXTURE_PLAYER,      "static/game/Player_4x3.png")
            .add(TEXTURE_LAVA,        "static/game/Lava.png")
            .add(TEXTURE_FIRE,        "static/game/Fire.png")
            .add(TEXTURE_ROCK,        "static/game/Rock.png")
            .load(() => this.onAssetsLoaded());
    }

    //! Setup scene with global components that remain for the whole game
    setupScene() {

        this.engine.scene.clearScene();
        this.engine.app.renderer.backgroundColor = BACKGROUND_COLOR;

        this.engine.scene.addGlobalComponent(new PhysicsEngineComponent);
        this.engine.scene.addGlobalComponent(new CameraComponent);
        this.engine.scene.addGlobalComponent(new InputComponent);
        this.engine.scene.addGlobalComponent(new GroundComponent);
        this.engine.scene.addGlobalComponent(new LevelManagerComponent);
        this.engine.scene.addGlobalComponent(new HUDComponent);
    }

    //! Sets up scene and block textures
    onAssetsLoaded() {
        // Game uses pixelated style, we want to avoid sampling interpolation
        let i : number;
        for (i = 0 | 0; i < this.blockTextures.length; i++) {
            let texture : Texture = Texture.fromImage(this.blockTextures[i][0]);
            texture.baseTexture.mipmap = false;
            texture.baseTexture.scaleMode = SCALE_MODES.NEAREST;
        }

        this.setupScene();
    }
}

new Game();
