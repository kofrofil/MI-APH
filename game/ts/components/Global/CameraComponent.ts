import { Rectangle, Point } from "pixi.js";
import { MSG_NOT_CAMERA_RECT_UPDATED, MSG_CMD_MOVE_CAMERA, MSG_CMD_ZOOM_CAMERA, MSG_CMD_RESET_CAMERA } from "../../Constants";
import Component from "../../../../ts/engine/Component";
import Msg from "../../../../ts/engine/Msg";

//! Global component simulating camera by stage offset.
export class CameraComponent extends Component {
    //! Camera X position from origin
    private posX : number = 0;
    private posY : number = 0;
    private width?: number = null;
    private height?: number = null;

    //! Zoom at given visible camera rectangle. This is not being applied to it. 
    private zoom : number = 1;

    //! Set camera at given position 
    private setPosition(x: number, y: number) {
        this.posX = x;
        this.posY = y;
    }

    //! Return visible camera rectangle (excluding zoom)
    private getVisibleRect() : Rectangle {
        let scaleX = 1;
        let scaleY = 1;
        let width = this.scene.app.screen.width;
        let height = this.scene.app.screen.height;
        if (this.width != null) {
            scaleX = this.scene.app.screen.width / this.width;
            width = this.width;
        }
        if (this.height != null) {
            scaleY = this.scene.app.screen.height / this.height;
            height = this.height;
        }
        return new Rectangle(this.posX * scaleX, this.posY * scaleY, width, height);
    }

    onInit() {
        this.subscribe(MSG_CMD_MOVE_CAMERA);
        this.subscribe(MSG_CMD_ZOOM_CAMERA);
        this.subscribe(MSG_CMD_RESET_CAMERA);
    }

    onRemove() {
        this.unsubscribe(MSG_CMD_MOVE_CAMERA);
        this.unsubscribe(MSG_CMD_ZOOM_CAMERA);
        this.unsubscribe(MSG_CMD_RESET_CAMERA);
    }

    onMessage(msg: Msg) {
		if (msg.action == MSG_CMD_MOVE_CAMERA) {
            let point = <Point>msg.data;
            this.setPosition(point.x, point.y);
        }
        if (msg.action == MSG_CMD_ZOOM_CAMERA) {
            this.zoom = <number>msg.data;
        }
        if (msg.action == MSG_CMD_RESET_CAMERA) {
            // Zoom reset is needed
            this.zoom = 1;
            this.posX = 0;
            this.posY = 0;
            this.width = null;
            this.height = null;
        }
	}

    //! Updates stage by the current position and sends out event about the change
    onUpdate(delta: number, absolute: number) {
        let scaleX = this.width == null ? 1 : this.scene.app.screen.width / this.width;
        let scaleY = this.height == null ? scaleX : this.scene.app.screen.height / this.height;
        
        scaleX /= this.zoom;
        scaleY /= this.zoom;

        let posX = (this.posX + 0.5 * this.scene.app.screen.width * (1 - this.zoom)) * scaleX;
        let posY = (this.posY + 0.5 * this.scene.app.screen.height * (1 - this.zoom)) * scaleY;


        this.scene.stage.getPixiObj().scale.set(scaleX, scaleY);
        this.scene.stage.getPixiObj().position.set(-posX, -posY);

        this.sendMessage(MSG_NOT_CAMERA_RECT_UPDATED, this.getVisibleRect());
    }
}
