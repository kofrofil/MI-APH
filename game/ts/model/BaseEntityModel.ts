import { Point } from "pixi.js";

//! Abstract model for all entities with basic details needed for interaction
export abstract class BaseEntityModel {
    //! Gets centered X coordinate
    abstract getX() : number;
    //! Gets centered T coordinate
    abstract getY() : number;
    abstract getWidth() : number;
    abstract getHeight() : number;
    abstract getVelocity() : Point;
    //! Returns whether entity has a ground contact
    abstract getGroundContact() : boolean;
}
