import { PIXICmp } from "../../ts/engine/PIXIObject";

//! Describes the cause for damage
export enum DamageCause {
    Burn //< Damage caused by burning
}

//! Describes an object damage with amount and cause
export class ObjectDamage {
    object: PIXICmp.ComponentObject;
    damage: number;
    cause: DamageCause;

    constructor(object: PIXICmp.ComponentObject, damage: number, cause: DamageCause) {
        this.object = object;
        this.damage = damage;
        this.cause = cause;
    }
}
