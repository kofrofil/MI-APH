import Component from "../../../../ts/engine/Component";
import { PIXICmp } from "../../../../ts/engine/PIXIObject";
import Msg from "../../../../ts/engine/Msg";
import { MSG_NOT_CAMERA_RECT_UPDATED, TEXTURE_BLOCK_GRASS, MSG_CMD_SETUP_HUD, MSG_NOT_PLAYER_HEALTH_CHANGED, MSG_LVL_UPDATED, MSG_NOT_PLAYER_DEATH, MSG_NOT_PLAYER_REACHED_FINISH } from "../../Constants";
import { Rectangle } from "pixi.js";

//! Stats + Death/Finish HUD handler component
export class HUDComponent extends Component {

    private statusStyle = new PIXI.TextStyle({ fontFamily: 'Arvo', fontVariant: "small-caps", fontSize: 13, fontWeight: "bold", fill: '#eeaa00', align: 'center', stroke: '#222222', strokeThickness: 1 });
    private healthText = new PIXICmp.Text("HUD_Health", "");
    private levelText = new PIXICmp.Text("HUD_Level", "");

    private deathStyle = new PIXI.TextStyle({ fontFamily: 'Arvo', fontVariant: "small-caps", fontSize: 25, fontWeight: "bold", fill: '#ee3311', align: 'center', stroke: '#773300', strokeThickness: 4 });
    private deathText = new PIXICmp.Text("HUD_Death", "Wasted!");

    private finishStyle = new PIXI.TextStyle({ fontFamily: 'Arvo', fontVariant: "small-caps", fontSize: 25, fontWeight: "bold", fill: '#33EE22', align: 'center', stroke: '#222222', strokeThickness: 4 });
    private finishText = new PIXICmp.Text("HUD_Finish", "Safety reached!");

    //! Remembers last announced level to not rely on message update 
    private lastLevel = 1;

    onInit() {
        this.subscribe(MSG_NOT_CAMERA_RECT_UPDATED);
        this.subscribe(MSG_CMD_SETUP_HUD);
        this.subscribe(MSG_NOT_PLAYER_HEALTH_CHANGED);
        this.subscribe(MSG_LVL_UPDATED);
        this.subscribe(MSG_NOT_PLAYER_DEATH);
        this.subscribe(MSG_NOT_PLAYER_REACHED_FINISH);
    }

    onRemove() {
        this.unsubscribe(MSG_NOT_CAMERA_RECT_UPDATED);
        this.unsubscribe(MSG_CMD_SETUP_HUD);
        this.unsubscribe(MSG_NOT_PLAYER_HEALTH_CHANGED);
        this.unsubscribe(MSG_LVL_UPDATED);
        this.unsubscribe(MSG_NOT_PLAYER_DEATH);
        this.unsubscribe(MSG_NOT_PLAYER_REACHED_FINISH);
    }

    onUpdate(delta: number, absolute: number) {
    }

    //! Position UI elements according to given camera rectangle
    private handleCameraRectUpdate(rect : Rectangle) {
        this.healthText.getPixiObj().x = rect.x;
        this.healthText.getPixiObj().y = rect.y + rect.height - this.healthText.getPixiObj().height;

        this.levelText.getPixiObj().x = rect.x + rect.width - this.levelText.getPixiObj().width;
        this.levelText.getPixiObj().y = rect.y + rect.height - this.levelText.getPixiObj().height;

        this.deathText.getPixiObj().x = rect.x + rect.width * 0.5 - this.deathText.getPixiObj().width * 0.5;
        this.deathText.getPixiObj().y = rect.y + rect.height * 0.5 - this.deathText.getPixiObj().height * 0.5;

        this.finishText.getPixiObj().x = rect.x + rect.width * 0.5 - this.finishText.getPixiObj().width * 0.5;
        this.finishText.getPixiObj().y = rect.y + rect.height * 0.5 - this.finishText.getPixiObj().height * 0.5;
    }

    //! Create all UI elements to the scene and add permanent ones
    private setup() {
        (<PIXI.Text>this.healthText.getPixiObj()).style = this.statusStyle;
        this.scene.stage.getPixiObj().addChild(this.healthText);

        (<PIXI.Text>this.levelText.getPixiObj()).style = this.statusStyle;
        this.scene.stage.getPixiObj().addChild(this.levelText);

        (<PIXI.Text>this.deathText.getPixiObj()).style = this.deathStyle;

        (<PIXI.Text>this.finishText.getPixiObj()).style = this.finishStyle;

        this.updatePlayerHealth(1);
        this.updateLevel(this.lastLevel);
    }
    
    private updateLevel(level: number) {
        (<PIXI.Text>this.levelText.getPixiObj()).text = "level: " + level;
        this.lastLevel = level;
    }

    private updatePlayerHealth(health: number) {
        (<PIXI.Text>this.healthText.getPixiObj()).text = "health: " + Math.max(Math.ceil(health * 100), 0) + "%";
    }

    //! Adds death text to the scene
    private showPlayerDied() {
        this.scene.stage.getPixiObj().addChild(this.deathText);
    }

    //! Adds finish text to the scene
    private showPlayerFinished() {
        this.scene.stage.getPixiObj().addChild(this.finishText);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_NOT_CAMERA_RECT_UPDATED) {
            this.handleCameraRectUpdate(<Rectangle>msg.data);
        }
        if (msg.action == MSG_CMD_SETUP_HUD) {
            this.setup();
        }
        if (msg.action == MSG_NOT_PLAYER_HEALTH_CHANGED) {
            this.updatePlayerHealth(<number>(msg.data));
        }
        if (msg.action == MSG_LVL_UPDATED) {
            this.updateLevel(<number>msg.data);
        }
        if (msg.action == MSG_NOT_PLAYER_DEATH) {
            this.showPlayerDied();
        }
        if (msg.action == MSG_NOT_PLAYER_REACHED_FINISH) {
            this.showPlayerFinished();
        }
    }

}
