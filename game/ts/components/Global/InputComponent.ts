import Component from "../../../../ts/engine/Component";

// Component storing key states.
export class InputComponent extends Component {
    //! Set of currently pressed keys
    private keys = new Set<number>();

    onInit() {
        document.addEventListener("keyup", this.onKeyUp.bind(this), false);
        document.addEventListener("keydown", this.onKeyDown.bind(this), false);
    }

    onRemove() {
        document.removeEventListener("keyup", this.onKeyUp.bind(this));
        document.removeEventListener("keydown", this.onKeyDown.bind(this));
    }

    //! Checks if given key is now pressed
    isKeyPressed(keyCode: number) : boolean {
        return this.keys.has(keyCode);
    }

    //! Callback when a key is pressed down
    private onKeyDown(evt: KeyboardEvent) {
        this.keys.add(evt.keyCode);
    }

    //! Callback when a key is released
    private onKeyUp(evt: KeyboardEvent) {
        this.keys.delete(evt.keyCode);
    }
}
