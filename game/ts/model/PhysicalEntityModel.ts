import { BaseEntityModel } from "./BaseEntityModel";
import { PIXICmp } from "../../../ts/engine/PIXIObject";

//! Model of physical entity storing its physical matter-js body and making its properties available in abstraction
export class PhysicalEntityModel extends BaseEntityModel {
    private body : Matter.Body = null;
    private object : PIXICmp.ComponentObject = null;
    private groundContact: boolean = false;

    constructor(object : PIXICmp.ComponentObject) { 
        super();
        this.object = object;
    }

    getBody() : Matter.Body {
        return this.body;
    }

    setBody(body : Matter.Body) {
        this.body = body;
    }

    setGroundContact(contact: boolean) {
        this.groundContact = contact;
    }

    // ==== Implemented interace ====
    getX() : number {
        return this.body.position.x;
    }

    getY() : number {
        return this.body.position.y;
    }

    getWidth(): number {
        return this.object.getPixiObj().width;
    }

    getHeight(): number {
        return this.object.getPixiObj().height;
    }

    getVelocity(): PIXI.Point {
        return new PIXI.Point(this.body.velocity.x, this.body.velocity.y);
    }

    getGroundContact(): boolean {
        return this.groundContact;
    }
}
