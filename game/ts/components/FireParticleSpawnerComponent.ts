import Component from "../../../ts/engine/Component";
import { MSG_NOT_ENTITY_DAMAGE, PARTICLES_PER_SECOND, PARTICLES_PER_CONTAINER, TEXTURE_FIRE, ATTR_BASE_ENTITY_MODEL, FIRE_SIZE } from "../Constants";
import Msg from "../../../ts/engine/Msg";
import { ObjectDamage, DamageCause } from "../ObjectDamage";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { BaseEntityModel } from "../model/BaseEntityModel";

//! Spawner for fire particles on damage message. It pools particles (non-component objects for perf. reasons) that can be drawn.
export class FireParticleSpawnerComponent extends Component {
    //! Efficient particle object drawing
    private particleContainer = new PIXICmp.ParticleContainer;
    //! Cached particles
    private particlePool : PIXI.Sprite[] = [];
    //! Map of how long each sprite should remain on screen
    private spriteToTimeToLive = new Map<PIXI.DisplayObject, number>();
    //! Time for which particles should be generated
    private generateFireTimeout = 0;
    //! Number of particles to generate during above time
    private generateFireCounter = 0;

    onInit() {
        this.subscribe(MSG_NOT_ENTITY_DAMAGE);
        this.scene.stage.getPixiObj().addChild(this.particleContainer);

        let tex = PIXI.Texture.fromImage(TEXTURE_FIRE);
        let scale = FIRE_SIZE / tex.baseTexture.width;
        // Populate particle pool
        for (let i = 0; i < PARTICLES_PER_CONTAINER; i++) {
            let sprite = new PIXI.Sprite(tex);
            sprite.anchor.set(0.5);
            sprite.scale.x = scale;
            sprite.scale.y = scale;
            this.particlePool.push(sprite);
        }
    }

    onRemove() {
        this.unsubscribe(MSG_NOT_ENTITY_DAMAGE);
        this.particlePool.splice(0, this.particlePool.length);
        this.scene.stage.getPixiObj().removeChild(this.particleContainer);
    }

    onUpdate(delta: number, absolute: number) {
        let toRemove : PIXI.DisplayObject[] = [];

        // Simulate particles on screen
        for (let sprite of this.particleContainer.getPixiObj().children) {
            let time = this.spriteToTimeToLive.get(sprite);
            // Remove too old particles
            if (time <= 0) {
                toRemove.push(sprite);
            }
            let val = Math.min(Math.floor(time * 256), 255);
            val = Math.max(Math.floor(time * 256), 0);
            // Apply gradual tint and fade-out using alpha
            let ssprite = (<PIXI.Sprite>sprite);
            ssprite.tint = val * 256 * 256 * 256 + val * 256 * 256 + val * 256 + 0;
            ssprite.alpha = val / 255;
            this.spriteToTimeToLive.set(sprite, time - delta);
        }
        for (let sprite of toRemove) {
            this.particleContainer.getPixiObj().removeChild(sprite);
            this.particlePool.push(<PIXI.Sprite>sprite);
        }

        if (this.generateFireTimeout > 0) {
            this.generateFireParticles();
            this.generateFireCounter += delta;
        } else {
            this.generateFireCounter = 0;
        }
        this.generateFireTimeout -= delta;
    }
    
    //! Simulates generation of particles
    private generateFireParticles() {
        let model = this.owner.getAttribute<BaseEntityModel>(ATTR_BASE_ENTITY_MODEL);
        let particlesToFire = Math.floor(this.generateFireCounter * PARTICLES_PER_SECOND);
        this.generateFireCounter -= particlesToFire / PARTICLES_PER_SECOND;

        for (let i = 0; i < particlesToFire; i++) {
            if (this.particlePool.length < 1)
                return;

            let sprite = this.particlePool.splice(this.particlePool.length - 1, 1)[0];
            sprite.position.x = model.getX() + (Math.random() - 0.5) * model.getWidth();
            sprite.position.y = model.getY() +( Math.random() - 0.5) * model.getHeight();
            this.spriteToTimeToLive.set(sprite, 1);
            this.particleContainer.getPixiObj().addChild(sprite);
        }
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_NOT_ENTITY_DAMAGE) {
            let damage = <ObjectDamage>msg.data;
            if (damage.object == this.owner) {
                switch(damage.cause) {
                    case DamageCause.Burn:
                        this.generateFireTimeout = damage.damage;
                        break;
                }
            }
        }
    }

}