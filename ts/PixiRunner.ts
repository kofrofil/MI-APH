import * as PIXI from 'pixi.js'
import Scene from './engine/Scene';
import '../libs/pixi-sound/pixi-sound.js';

/**
 * Entry point to the PIXIJS
 */
export class PixiRunner {
    app: PIXI.Application = null;
    lastTime = 0;
    gameTime = 0;
    scene: Scene = null;
    ticker: PIXI.ticker.Ticker = null;

    init(canvas: HTMLCanvasElement, resolution: number = 1) {
        console.log("Initial canvas size: " + canvas.width + " x " + canvas.height)
        console.log("Initial resolution: " + resolution)

        this.app = new PIXI.Application({
            width: canvas.width / resolution,
            height: canvas.height / resolution,
            antialias: false,
            view: canvas,
            // autoStart: false, // Don't eat everything when slowing down.
            resolution: resolution, // resolution/device pixel ratio
            backgroundColor: 0x111111
        });

        this.app.renderer.autoResize = false;
        var onResize = () =>
        {
            this.app.screen.width = window.innerWidth;
            this.app.screen.height = window.innerHeight;
            this.app.renderer.resize(window.innerWidth / this.app.renderer.resolution, window.innerHeight / this.app.renderer.resolution);
            console.log("Window resized: " + window.innerWidth + " x " + window.innerHeight)
        };
        window.addEventListener("resize", onResize);
        onResize();

        this.scene = new Scene(this.app);
        this.ticker = PIXI.ticker.shared;
        // stop the shared ticket and update it manually
        this.ticker.autoStart = false;
        this.ticker.stop();

        this.loop(performance.now());
    }

    private loop(time) {
        // update our component minilib
        let dt = (time - this.lastTime);
        this.lastTime = time;
        // Stabilize delta
        dt = Math.max(0.001, dt);
        dt = Math.min(1, dt);
        this.gameTime += dt;
        this.scene._update(dt, this.gameTime);

        // update PIXI
        this.ticker.update(this.gameTime);
        requestAnimationFrame((time) => this.loop(time * 0.001));
        // setTimeout(() => requestAnimationFrame((time) => this.loop(time)), 1000);
    }
}
