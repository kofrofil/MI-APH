import Component from "../../../ts/engine/Component";
import { Sprite, Rectangle } from "pixi.js";
import { PLAYER_WIDTH, MSG_NOT_OBJECT_SCALE_CHANGED, ATTR_BASE_ENTITY_MODEL, ANIMATION_VELOCITY_THRESHOLD, ANIMATION_SPEED } from "../Constants";
import { BaseEntityModel } from "../model/BaseEntityModel";

//! Component animating sprite if texture with multiple frames given (stored in name as TEXTURE_COLUMNSxROWS.EXTENSION) according to movement only
export class SpriteAnimationComponent extends Component {
    //! Number of frame columns in the texture
    private columns: number = 1;
    //! Number of frame rows in the texture
    private rows: number = 1;
    //! Number of frames (columns * rows)
    private maxFrames: number = 1;
    //! Current frame
    private frame: number = 0;
    //! Calculated scale for texture frame
    private scale: number = 1;
    //! Flipping of texture according to direction
    private directionForward: boolean = true;

    onInit() {
        // Extract columns and rows info from the texture file name
        let sprite = (<Sprite>this.owner.getPixiObj());
        let baseTexture = sprite.texture.baseTexture;
        let src = baseTexture.source.getAttribute("src").split("/");
        if (src.length > 0) {
            let fileParts = src[src.length - 1].split(".");
            if (fileParts.length > 0) {
                let fileNameParts = fileParts[0].split("_");
                if (fileNameParts.length > 1) {
                    let dimensions = fileNameParts[fileNameParts.length - 1].split("x");
                    if (dimensions.length == 2) {
                        this.columns = parseInt(dimensions[0]);
                        this.rows = parseInt(dimensions[1]);
                    }
                }
            }
        }
        this.maxFrames = this.columns * this.rows;
        // Set default frame
        sprite.texture.frame = new Rectangle(0, 0, baseTexture.width / this.columns, baseTexture.height / this.rows);
        // Size according to 
        this.scale = PLAYER_WIDTH / (sprite.texture.baseTexture.width / this.columns);
        sprite.scale.set(this.scale, this.scale);
        this.sendMessage(MSG_NOT_OBJECT_SCALE_CHANGED);
    }

    //! Apply offsets for current frame
    private applyFrame() {
        let frameY = Math.floor(this.frame / this.columns);
        let frameX = Math.floor(this.frame - frameY * this.columns);

        let sprite = (<Sprite>this.owner.getPixiObj());
        sprite.texture.frame.x = frameX * sprite.texture.frame.width;
        sprite.texture.frame.y = frameY * sprite.texture.frame.height;
        sprite.texture.frame = new Rectangle(sprite.texture.frame.x, sprite.texture.frame.y, sprite.texture.frame.width, sprite.texture.frame.height);

        sprite.scale.set(this.scale * (this.directionForward ? 1 : -1), this.scale);
    }

    //! Simulate animation according to time
    private animate(delta: number) {
        this.frame += delta * ANIMATION_SPEED;
        if (this.frame >= this.maxFrames) { 
            this.frame = 0;
        }

        this.applyFrame();
    }

    //! Reset animation to default frame 0
    private cancelAnimation() {
        this.frame = 0;
        this.applyFrame();
    }

    //! Simulate animation according to velocity
    onUpdate(delta: number, absolute: number) {
        let baseModel = this.owner.getAttribute<BaseEntityModel>(ATTR_BASE_ENTITY_MODEL);
        let velocity = baseModel.getVelocity();
        if (Math.abs(velocity.x) > ANIMATION_VELOCITY_THRESHOLD) {
            this.animate(delta);
            this.directionForward = velocity.x > 0;
        } else {
            this.cancelAnimation();
        }
    }

    onRemove() {
    }
}
