//! Class for any utility funciton
export class Utils {
    //! Returns whether two floating point numbers are almost the same with 1e-12 precision
    static almostSameNumber(a : number, b : number) : boolean {
        return Math.abs(a - b) < 1e-12;
    }
}
