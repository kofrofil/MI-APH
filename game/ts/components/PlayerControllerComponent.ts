import Component from "../../../ts/engine/Component";
import { InputComponent } from "./Global/InputComponent";
import { PhysicalEntityModel } from "../model/PhysicalEntityModel";
import { ATTR_PHYSICAL_ENTITY_MODEL, PLAYER_MOVE_FORCE_SIDEWAYS, PLAYER_MOVE_FORCE_UP, MSG_NOT_COLLISION_ACTIVE, MSG_NOT_COLLISION_START, PLAYER_STOPING_FORCE_COEF, PLAYER_STOPING_VELOCITY_THRESHOLD, PLAYER_JUMP_TIMEOUT, PLAYER_JUMP_IMPULSE_FORCE_SIDEWAY, PLAYER_JUMP_IMPULSE_FORCE_UP, MSG_NOT_ENTITY_DAMAGE, ATTR_PLAYER_MODEL, MSG_NOT_PLAYER_HEALTH_CHANGED, MSG_NOT_PLAYER_DEATH, ATTR_GROUND_OPTIONS, MSG_NOT_PLAYER_REACHED_FINISH, PLAYER_COLLISION_DAMAGE, KEY_A, KEY_LEFT, KEY_D, KEY_RIGHT, KEY_SPACE } from "../Constants";
import Matter from "matter-js";
import Msg from "../../../ts/engine/Msg";
import { CollisionMessage } from "./Global/PhysicsEngineComponent";
import { Rectangle } from "pixi.js";
import { ObjectDamage as EntityDamage } from "../ObjectDamage";
import { PlayerModel } from "../model/PlayerModel";
import { GroundOptions } from "../GroundParts";

//! Player input control, health simulation and player collision handler (TODO: This should be its own component)
export class PlayerControllerComponent extends Component {
    //! Checks if a ground collision occured at the current (the latest)
    private collisionGroundNoticedThisFrame: boolean = false;
    //! Time before player can jump again
    private jumpTimeout: number = 0;
    //! Averaged time for 2 frames delta for impulse calculation
    private avgDelta: number = 0;

    onInit() {
        this.subscribe(MSG_NOT_COLLISION_ACTIVE);
        this.subscribe(MSG_NOT_COLLISION_START);
        this.subscribe(MSG_NOT_ENTITY_DAMAGE);
    }

    onRemove() {
        this.unsubscribe(MSG_NOT_COLLISION_ACTIVE);
        this.unsubscribe(MSG_NOT_COLLISION_START);
        this.unsubscribe(MSG_NOT_ENTITY_DAMAGE);
    }

    onUpdate(delta: number, absolute: number) {

        this.avgDelta = (this.avgDelta + delta) * 0.5;
        let input = <InputComponent>this.scene.stage.findComponentByClass(InputComponent.name);
        let physModel = this.owner.getAttribute<PhysicalEntityModel>(ATTR_PHYSICAL_ENTITY_MODEL);
        let playerModel = this.owner.getAttribute<PlayerModel>(ATTR_PLAYER_MODEL);

        // Remember ground contant in the model
        physModel.setGroundContact(this.collisionGroundNoticedThisFrame || physModel.getBody().isSleeping);
        if (physModel.getGroundContact() && playerModel.health > 0) {
            // First collect all forces
            let forceX = 0;
            let forceY = 0;
    
            // Handle key input
            if (input.isKeyPressed(KEY_A) || input.isKeyPressed(KEY_LEFT)) {
                forceX += -PLAYER_MOVE_FORCE_SIDEWAYS;
                forceY += -PLAYER_MOVE_FORCE_UP;
            }
    
            if (input.isKeyPressed(KEY_D) || input.isKeyPressed(KEY_RIGHT)) {
                forceX += PLAYER_MOVE_FORCE_SIDEWAYS;
                forceY += -PLAYER_MOVE_FORCE_UP;
            }

            if (input.isKeyPressed(KEY_SPACE)) {
                if (this.jumpTimeout < 0) {
                    if (forceX != 0) {
                        forceX += PLAYER_JUMP_IMPULSE_FORCE_SIDEWAY * Math.sign(forceX);
                    }
                    forceY += -PLAYER_JUMP_IMPULSE_FORCE_UP;
                    this.jumpTimeout = PLAYER_JUMP_TIMEOUT;
                }
            }

            if (forceX == 0 && forceY == 0) {
                if (Math.abs(physModel.getVelocity().x) > PLAYER_STOPING_VELOCITY_THRESHOLD) {
                    // Stop the laterral movement (approximation)
                    forceX += PLAYER_STOPING_FORCE_COEF / delta * -physModel.getVelocity().x * this.avgDelta;

                    // Since this is an approximation only, limit the values a little
                    forceX = Math.min(1.0, forceX);
                    forceX = Math.max(-1.0, forceX);
                }
            }

            if (forceX != 0 || forceY != 0) {
                // Apply final force to matter-js as impulse for current time that should be applied for 1 second (but just for this delta)
                Matter.Body.applyForce(physModel.getBody(), Matter.Vector.create(physModel.getX(), physModel.getY()), Matter.Vector.create(forceX / this.avgDelta, forceY / this.avgDelta));
            }
        }

        let groundOptions = this.scene.getGlobalAttribute<GroundOptions>(ATTR_GROUND_OPTIONS);
        if (physModel.getY() > groundOptions.finishHeight) {
            // Send that we reached the finish
            this.sendMessage(MSG_NOT_PLAYER_REACHED_FINISH);
        }

        this.jumpTimeout -= delta;
        this.collisionGroundNoticedThisFrame = false;
    }

    private handleCollisionMsg(collMsg: CollisionMessage) {
        let rect : Rectangle= null;
        if (collMsg.object != this.owner) {
            return;
        }
        if (collMsg.withGround != null) {
            rect = collMsg.withGround;
        }
        if (collMsg.withObject != null) {
            rect = collMsg.withObject.getPixiObj().getBounds();
            this.handleEntityDamage(PLAYER_COLLISION_DAMAGE);
        }
        if (rect != null) {
            let physModel = this.owner.getAttribute<PhysicalEntityModel>(ATTR_PHYSICAL_ENTITY_MODEL);

            // If bellow player center, consider as ground
            if (rect.y > physModel.getY() + physModel.getHeight() * 0.5) {
                this.collisionGroundNoticedThisFrame = true;
            }
        }
    }

    //! Decrease health and check for death
    private handleEntityDamage(damage: number) {
        let playerModel = this.owner.getAttribute<PlayerModel>(ATTR_PLAYER_MODEL);
        let prevHealth = playerModel.health;
        playerModel.health -= damage;
        this.sendMessage(MSG_NOT_PLAYER_HEALTH_CHANGED, playerModel.health);
        if (prevHealth > 0 && playerModel.health <= 0) {
            this.sendMessage(MSG_NOT_PLAYER_DEATH);
        }
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_NOT_COLLISION_START) {
            this.handleCollisionMsg(<CollisionMessage>msg.data);
        }
        if (msg.action == MSG_NOT_COLLISION_ACTIVE) {
            this.handleCollisionMsg(<CollisionMessage>msg.data);
        }
        if (msg.action == MSG_NOT_ENTITY_DAMAGE) {
            let damage = <EntityDamage>msg.data
            if (damage.object == this.owner) {
                this.handleEntityDamage(damage.damage);
            }
        }
    }
}
