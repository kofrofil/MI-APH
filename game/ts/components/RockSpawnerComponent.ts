import Component from "../../../ts/engine/Component";
import { SceneFactory } from "../SceneFactory";
import { Point } from "pixi.js";
import { BaseEntityModel } from "../model/BaseEntityModel";
import { ATTR_BASE_ENTITY_MODEL, ROCK_SHOT_FORCE_X, ROCK_SHOT_FORCE_Y, ROCK_SPAWN_TIMEOUT } from "../Constants";

//! Rock spawning component shooting them in a static but randomized orientation
export class RockSpawnerComponent extends Component {

    // Timeout before next rock spawn
    private spawnTimeout = 1;
    private sceneFactory: SceneFactory = null;

    onInit() {
        this.sceneFactory = new SceneFactory(this.scene);
    }

    onUpdate(delta: number, absolute: number) {
        let model = this.owner.getAttribute<BaseEntityModel>(ATTR_BASE_ENTITY_MODEL);

        this.spawnTimeout -= delta;
        if (this.spawnTimeout < 0) {
            this.spawnTimeout = ROCK_SPAWN_TIMEOUT;

            // Calculate random direction of impulse force
            let forceX = Math.random() * ROCK_SHOT_FORCE_X;
            let forceY = -Math.random() * ROCK_SHOT_FORCE_Y;

            // Spawn the actual rock
            this.sceneFactory.spawnRock(new Point(model.getX() + 200, model.getY() - model.getHeight()), new Point(forceX, forceY));
        }
	}
}
