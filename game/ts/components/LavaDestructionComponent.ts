import Component from "../../../ts/engine/Component";
import { FLAG_DESTRUCTIBLE, MSG_NOT_ENTITY_DAMAGE, LAVA_DAMAGE_PER_SECOND, ATTR_BASE_ENTITY_MODEL } from "../Constants";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { BaseEntityModel } from "../model/BaseEntityModel";
import { ObjectDamage, DamageCause } from "../ObjectDamage";

//! Checks collisions with all entities and sends out damage messages as it is not a physical entity
export class LavaDestructionComponent extends Component {

    private hasCollisionWith(entity: PIXICmp.ComponentObject): boolean {
        let entityModel = entity.getAttribute<BaseEntityModel>(ATTR_BASE_ENTITY_MODEL);
        let lavaModel = this.owner.getAttribute<BaseEntityModel>(ATTR_BASE_ENTITY_MODEL)

        let entityL = entityModel.getX() - entityModel.getWidth() * 0.5;
        let entityR = entityModel.getX() + entityModel.getWidth() * 0.5;

        //let entityT = entityModel.getY() - entityModel.getHeight() * 0.5;
        //let entityB = entityModel.getY() + entityModel.getHeight() * 0.5;

        let lavaL = lavaModel.getX() - lavaModel.getWidth() * 0.5;
        let lavaR = lavaModel.getX() + lavaModel.getWidth() * 0.5;

        //let lavaT = lavaModel.getY() - lavaModel.getHeight() * 0.5;
        //let lavaB = lavaModel.getY() + lavaModel.getHeight() * 0.5;

        // rough collision, we ignore height as it could be used to survive on hills
        return entityR > lavaL && entityL < lavaR; // && entityB > lavaT && entityT < lavaB;
    }

    onUpdate(delta: number, absolute: number) {
		for (let entity of this.scene.findAllObjectsByFlag(FLAG_DESTRUCTIBLE)) {
            if (entity == this.owner)
                continue;

            if (this.hasCollisionWith(entity)) {
                this.sendMessage(MSG_NOT_ENTITY_DAMAGE, new ObjectDamage(entity, LAVA_DAMAGE_PER_SECOND * delta, DamageCause.Burn));
            }
        }
	}

}
