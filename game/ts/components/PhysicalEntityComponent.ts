import Component from "../../../ts/engine/Component";
import { MSG_NOT_PHYSICAL_OBJECT_CREATED, MSG_NOT_PHYSICAL_OBJECT_REMOVED, ATTR_PHYSICAL_ENTITY_MODEL, ATTR_BASE_ENTITY_MODEL, MSG_NOT_OBJECT_SCALE_CHANGED, PLAYER_MASS } from "../Constants";
import Matter from "matter-js";
import { PhysicalEntityModel } from "../model/PhysicalEntityModel";
import Msg from "../../../ts/engine/Msg";

//! Represents entity interacting with physical world as circle with only translation used
export class PhysicalEntityComponent extends Component {
    private mass: number;

    constructor(mass: number) {
        super();
        this.mass = mass;
    }

    //! Creates matter-js body according to sprite parameters and announces it
    private createBody() {
        let pos = this.owner.getPixiObj().position;
        this.owner.getPixiObj().calculateBounds();
        let bounds = this.owner.getPixiObj().getBounds();
        let size = Math.max(bounds.width, bounds.height) / 2;
        let body = Matter.Bodies.circle(pos.x, pos.y, size, { restitution: 0.05, friction: 0.6, frictionAir: 0.01, frictionStatic: 0.1, mass: this.mass }, 30 );
        this.sendMessage(MSG_NOT_PHYSICAL_OBJECT_CREATED, body);
        this.owner.getAttribute<PhysicalEntityModel>(ATTR_PHYSICAL_ENTITY_MODEL).setBody(body);
    }

    //! Destroys matter-js body and announces it
    private destroyBody() {
        let body = this.owner.getAttribute<PhysicalEntityModel>(ATTR_PHYSICAL_ENTITY_MODEL).getBody();
        this.sendMessage(MSG_NOT_PHYSICAL_OBJECT_REMOVED, body);
    }

    onInit() {
        this.subscribe(MSG_NOT_OBJECT_SCALE_CHANGED);
        this.createBody();
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_NOT_OBJECT_SCALE_CHANGED && msg.gameObject == this.owner) {
            this.destroyBody();
            this.createBody();
        }
    }

    onUpdate(delta: number, absolute: number) {
        // Position sprite accoridng to the physical model, ignore orientation - it is used only for movement
        let model = this.owner.getAttribute<PhysicalEntityModel>(ATTR_PHYSICAL_ENTITY_MODEL);
        this.owner.getPixiObj().position.x = model.getX();
        this.owner.getPixiObj().position.y = model.getY();
    }

    onRemove() {
        this.destroyBody();
        this.unsubscribe(MSG_NOT_OBJECT_SCALE_CHANGED);
    }
}
