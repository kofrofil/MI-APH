import { BaseEntityModel } from "./BaseEntityModel";
import { PLAYER_MOVE_FORCE_SIDEWAYS, PLAYER_MASS, LAVA_WIDTH, LAVA_HEIGHT, LAVA_SPEED } from "../Constants";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { Point } from "pixi.js";

//! Lava model with its specific positional properties to account for different sprite sizing/positioning 
export class LavaEntityModel extends BaseEntityModel {
    private object : PIXICmp.ComponentObject = null;

    constructor(object : PIXICmp.ComponentObject) { 
        super();
        this.object = object;
    }

    getX(): number {
        return this.object.getPixiObj().position.x + this.getWidth() * 0.5;
    }
    getY(): number {
        return this.object.getPixiObj().position.y;
    }
    getWidth(): number {
        return LAVA_WIDTH;
    }
    getHeight(): number {
        return LAVA_HEIGHT;
    }
    getVelocity(): PIXI.Point {
        return new Point(LAVA_SPEED, 0);
    }
    getGroundContact(): boolean {
        return true;
    }
}
