import Matter, { World, Events } from "matter-js";
import { MSG_NOT_GROUND_CHANGED, GRAVITY, MSG_NOT_PHYSICAL_OBJECT_CREATED, MSG_NOT_PHYSICAL_OBJECT_REMOVED, MSG_CMD_SETUP_GROUND, MSG_NOT_COLLISION_START, MSG_NOT_COLLISION_END, MSG_NOT_COLLISION_ACTIVE, BLOCK_SIZE } from "../../Constants";
import { GroundChange } from "../../GroundParts";
import { Rectangle } from "pixi.js";
import { Utils } from "../../Utils";
import Component from "../../../../ts/engine/Component";
import { PIXICmp } from "../../../../ts/engine/PIXIObject";
import Msg from "../../../../ts/engine/Msg";

//! Helper class for associating ground rectangle with physics body
class GroundBody {
    constructor(body : Matter.Body, rect : Rectangle) {
        this.body = body;
        this.rect = rect;
    }

    body : Matter.Body = null;
    rect : Rectangle = null;
}

//! Wrapper for collision details
export class CollisionMessage {
    //! Collision message type
    action: string = "";
    //! Object that has the collision
    object : PIXICmp.ComponentObject = null;
    //! Object with what the collision occured (optional)
    withObject : PIXICmp.ComponentObject = null;
    //! Ground rectangle with what the collision occured (optional)
    withGround : Rectangle = null;
}

//! Component updating the Matter-js engine, handling entity body registration + special rectangle registration for ground
export class PhysicsEngineComponent extends Component {

    private engine : Matter.Engine = null;
    //! All currently registered ground rectangles
    private groundRects : GroundBody[] = [];
    //! Delayed collision events to be processed in the main loop
    private collisionEvents : CollisionMessage[] = [];
    //! Mapping body to entity
    private bodyToObject = new Map<Matter.Body, PIXICmp.ComponentObject>();
    //! Mapping body to collision handlers (used just for removal)
    private bodyToCollisionHandlers = new Map<Matter.Body, any[]>();
    //! Mapping entity to body
    private objectToBody = new Map<PIXICmp.ComponentObject, Matter.Body>();
    //! Last time delta (frame time) for corrections in Matter-js
    private lastDelta = 1;

    onInit() {
        this.subscribe(MSG_NOT_GROUND_CHANGED);
        this.subscribe(MSG_NOT_PHYSICAL_OBJECT_CREATED);
        this.subscribe(MSG_NOT_PHYSICAL_OBJECT_REMOVED);

        this.engine = Matter.Engine.create({enableSleeping: true});
        this.engine.world.gravity.y = GRAVITY;
    }

    onRemove() {
        this.unsubscribe(MSG_NOT_GROUND_CHANGED);
        this.unsubscribe(MSG_NOT_PHYSICAL_OBJECT_CREATED);
        this.unsubscribe(MSG_NOT_PHYSICAL_OBJECT_REMOVED);
    }

    //! Resolve if collision is with entity or ground and create collision event
    private handleCollisionBody(action: string, bodyObject: Matter.Body, anotherBody: Matter.Body) {
        let object = this.bodyToObject.get(bodyObject);
        let anotherObject = null;
        let ground = null;
        if (this.bodyToObject.has(anotherBody)) {
            anotherObject = this.bodyToObject.get(anotherBody);
        } else {
            ground = new Rectangle(anotherBody.position.x, anotherBody.position.y, BLOCK_SIZE, BLOCK_SIZE);
        }
        this.collisionEvents.push({ action: action, object: object, withObject: anotherObject, withGround: ground });
    }

    //! Resolve collision from all Matter events and create collision event for both sides of pairs
    private handleCollision(action: string, event: Matter.IEventCollision<Matter.Engine>) {
        for (let pair of event.pairs) {
            if (this.bodyToObject.has(pair.bodyA)) {
                this.handleCollisionBody(action, pair.bodyA, pair.bodyB);
            }
            if (this.bodyToObject.has(pair.bodyB)) {
                this.handleCollisionBody(action, pair.bodyB, pair.bodyA);
            }
        }
    }

    private handleCollisionStart(event: Matter.IEventCollision<Matter.Engine>) {
        this.handleCollision(MSG_NOT_COLLISION_START, event);
    }

    private handleCollisionActive(event: Matter.IEventCollision<Matter.Engine>) {
        this.handleCollision(MSG_NOT_COLLISION_ACTIVE, event);
    }

    private handleCollisionEnd(event: Matter.IEventCollision<Matter.Engine>) {
        this.handleCollision(MSG_NOT_COLLISION_END, event);
    }

	onMessage(msg: Msg) {
        if (msg.action == MSG_NOT_GROUND_CHANGED) {
            // Update ground rectangles according to given changes
            this.updateGround(<GroundChange> msg.data);
        }
        if (msg.action == MSG_NOT_PHYSICAL_OBJECT_CREATED) {
            // Register new physical object
            let body = <Matter.Body>msg.data;
            World.add(this.engine.world, body);
            this.bodyToObject.set(body, msg.gameObject);
            this.objectToBody.set(msg.gameObject, body);
            let handlerCollStart = (event : Matter.IEventCollision<Matter.Engine>) => { this.handleCollisionStart(event); };
            let handlerCollActive = (event : Matter.IEventCollision<Matter.Engine>) => { this.handleCollisionActive(event); };
            let handlerCollEnd = (event : Matter.IEventCollision<Matter.Engine>) => { this.handleCollisionEnd(event); };
            this.bodyToObject.set(body, msg.gameObject);
            let handlers = [handlerCollStart, handlerCollActive, handlerCollEnd];
            this.bodyToCollisionHandlers.set(body, handlers);
            Matter.Events.on(this.engine, "collisionStart", handlerCollStart);
            Matter.Events.on(this.engine, "collisionActive", handlerCollActive);
            Matter.Events.on(this.engine, "collisionEnd", handlerCollEnd);
        }
        if (msg.action == MSG_NOT_PHYSICAL_OBJECT_REMOVED) {
            // Unregister new physical object
            World.remove(this.engine.world, msg.data);
            let body = this.objectToBody.get(msg.gameObject);
            this.bodyToObject.delete(body);
            this.objectToBody.delete(msg.gameObject);
            let handlers =  this.bodyToCollisionHandlers.get(body);
            Matter.Events.off(this.engine, "collisionStart", handlers[0]);
            Matter.Events.off(this.engine, "collisionActive", handlers[1]);
            Matter.Events.off(this.engine, "collisionEnd", handlers[2]);
            this.bodyToCollisionHandlers.delete(body);
        }
    }

    onUpdate(delta: number, absolute: number) {
        Matter.Engine.update(this.engine, delta * 1000, delta / this.lastDelta);
        // Send all delayed events
        for (let collision of this.collisionEvents) { 
            this.sendMessage(collision.action, collision);
        }
        this.collisionEvents.splice(0, this.collisionEvents.length);
        this.lastDelta = delta;
    }

    //! Precisely remove and add bodies according to incoming ground changes
    private updateGround(groundChange : GroundChange) { 
        if (groundChange.removed != null) {
            for (let removedRect of groundChange.removed) {
                let idx = -1;
                for (let i = 0 | 0; i < this.groundRects.length; i += 1 | 0) {
                    let existRect = this.groundRects[i];
                    if (Utils.almostSameNumber(existRect.rect.x, removedRect.x)
                    && Utils.almostSameNumber(existRect.rect.y, removedRect.y)
                    && Utils.almostSameNumber(existRect.rect.width, removedRect.width)
                    && Utils.almostSameNumber(existRect.rect.height, removedRect.height)) {
                        idx = i;
                        break;
                    }
                }

                if (idx != -1) {
                    let removed = this.groundRects.splice(idx, 1);
                    Matter.World.remove(this.engine.world, removed[0].body);
                }
            }
        }

        if (groundChange.added != null) {
            for (let rect of groundChange.added) {
                let body = Matter.Bodies.rectangle(rect.x + rect.width * 0.5, rect.y + rect.height * 0.5, rect.width, rect.height, { isStatic: true });
                this.groundRects.push(new GroundBody(body, rect));
                Matter.World.add(this.engine.world, [body]);
            }
        } 
    }
}
