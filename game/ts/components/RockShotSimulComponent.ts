import Component from "../../../ts/engine/Component";
import { Point } from "pixi.js";
import { MSG_NOT_ENTITY_DAMAGE, ATTR_PHYSICAL_ENTITY_MODEL, ROCK_SPEED_PARTICLE_SPAWN_FACTOR, ROCK_TIME_TO_LIVE } from "../Constants";
import { ObjectDamage, DamageCause } from "../ObjectDamage";
import { PhysicalEntityModel } from "../model/PhysicalEntityModel";
import Matter from "matter-js";

//! Random (lava) rock simulation component. It requires physical mdoel
export class RockShotComponent extends Component {
    //! Impulse to be applied the first frame
    private initialImpulse: Point = null;
    //! Time to live for the object in the scene
    private timeoutExistence: number = ROCK_TIME_TO_LIVE;

    constructor(initialImpulse: Point) {
        super();
        this.initialImpulse = initialImpulse;
    }

    onUpdate(delta: number, absolute: number) {
        this.timeoutExistence -= delta;

        // Remove from the scene if too old
        if (this.timeoutExistence <= 0) {
            this.scene.stage.getPixiObj().removeChild(this.owner.getPixiObj());
        }

        let physModel = this.owner.getAttribute<PhysicalEntityModel>(ATTR_PHYSICAL_ENTITY_MODEL);
        if (this.initialImpulse) {
            // Apply initial impulse
            Matter.Body.applyForce(physModel.getBody(), Matter.Vector.create(physModel.getX(), physModel.getY()), Matter.Vector.create(this.initialImpulse.x / delta, this.initialImpulse.y / delta));
            this.initialImpulse = null;
        }

        // Apply physical orientation
        this.owner.getPixiObj().rotation = physModel.getBody().angle;

        let speed = Math.sqrt(physModel.getVelocity().y * physModel.getVelocity().y + physModel.getVelocity().x * physModel.getVelocity().x) * ROCK_SPEED_PARTICLE_SPAWN_FACTOR;
        // Simple hack: Rock is being damaged, it spawns burn particles
        this.sendMessage(MSG_NOT_ENTITY_DAMAGE, new ObjectDamage(this.owner, speed, DamageCause.Burn));
	}
}
